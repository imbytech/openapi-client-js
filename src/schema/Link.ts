import Server from "./Server";

interface Link {
  operationRef: string
  operationId: string
  // check the spec to understand the {expression} structure
  parameters: Map<string, any>
  requestBody: any
  description: string
  server: Server
}

export default Link;
