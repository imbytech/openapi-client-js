import ExternalDocumentation from "./ExternalDocumentation";

interface Tag {
  name: string
  description: string
  externalDocs: ExternalDocumentation
}

export default Tag;
