import MediaType from "./MediaType";

interface RequestBody {
  description: string
  content: Map<string, MediaType>
  required: boolean
}

export default RequestBody;
