interface ServerVariable {
  enum: Array<string>
  default: string
  description: string
}

export default ServerVariable;
