import Header from "./Header";
import Link from "./Link";
import MediaType from "./MediaType";
import Reference from "./Reference";

interface Response {
  description: string
  headers: Map<string, Header | Reference>
  content: Map<string, MediaType>
  links: Map<string, Link | Reference>
}

export default Response;
