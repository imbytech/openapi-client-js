import Header from "./Header";
import Reference from "./Reference";

interface Encoding {
  contentType: string
  headers: Map<string, Header | Reference>
  // works the same as in Parameter
  style: string
  explode: boolean
  allowReserved: boolean
}

export default Encoding;
