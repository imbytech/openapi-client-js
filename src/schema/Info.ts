import Contact from './Contact';
import License from './License';

interface Info {
  title: string
  summary: string
  description: string
  termsOfService: string
  contact: Contact
  license: License
  version: string
}

export default Info;
