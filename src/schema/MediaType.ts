import Encoding from "./Encoding";
import Example from "./Example";
import Schema from "./Schema";

interface MediaType {
  schema: Schema
  example: any
  examples: Map<string, Example>
  encodings: Map<string, Encoding>
}

export default MediaType;
