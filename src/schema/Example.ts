interface Example {
  summary: string
  description: string
  value: any
  externalValue: string
}

export default Example;
