import StatusError from './StatusError';
import RestResponse from './RestResponse';
import RestRequest from './RestRequest';

/**
 * @module ResponseHandler
 */

/**
 * Provide a handler that parses the response's body and handles the status
 * code as specified by the OpenApi configuration for the operation.
 * @alias module:ResponseHandler
 * @class
 */
export default class ResponseHandler {
  responses: object
  /**
   * Create a new handler based on responses configuration.
   * @param {object} responses the operation's responses configuration.
   */
  constructor(responses: object) {
    this.responses = responses || {};
  }

  /**
   * Process the response body.
   * @param {Response} response the HTTP response received from fetch
   * @param {RestRequest} request the request that fetched the response
   * @returns {RestResponse} the response wrapper with parsed body
   */
  processResponseBody(response: Response, request: RestRequest) {
    return RestResponse.fromRequestResponse(request, response);
  }

  /**
   * Process the underlying error body in the case of a status error.
   * @param {StatusError} error the status error
   * @returns {StatusError} the status error with processed body
   */
  processErrorBody(error: StatusError) {
    return error.withBody().then(e => Promise.reject(e));
  }

  /**
   * Process the response's status by checking if the status is an error
   * or success status. If it is a success status, it checks whether it is
   * defined in the operation's responses block and returns an error if
   * that block is defined and doesn't cover the actual status.
   * @param {RestRequest} request the request that fetched the response
   * @param {Response} response the response received from fetch
   */
  processStatus(request: RestRequest, response: Response) {
    // TODO: convert this into content handlers that have
    // a status selector and a processor
    let result = null;
    const status = response && response.status && response.status.toString();
    if (status) {
      const expected = this.responses && Object.keys(this.responses);
      const isOk = response.ok;
      let isExpected = true;
      if (expected && expected.length > 0) {
        isExpected = expected.includes(status) || expected.includes('default');
      }
      result = (isOk && isExpected)
        ? Promise.resolve(response)
        : Promise.reject(new StatusError(
          isExpected
            ? `API Error: ${status} - ${response.statusText}`
            : `Expected one of [${expected}], got: ${status} - ${response.statusText}`,
          request,
          response,
          expected
        ));
    } else {
      result = Promise.reject(new StatusError(
        'No status code',
        request,
        response
      ));
    }
    return result;
  }

  /**
   * No handling, simple pass through that returns the initial request.
   * @param {RestRequest} request the request to pass through
   */
  // eslint-disable-next-line class-methods-use-this
  prepareRequest(request: RestRequest) {
    return Promise.resolve(request);
  }

  /**
   * Handle the status and body of the response.
   * @param {Response} response the response received from fetch
   * @param {RestRequest} request the original request
   */
  // eslint-disable-next-line class-methods-use-this
  handleResponse(response: Response, request: RestRequest) {
    return this.processStatus(request, response).then(
      r => this.processResponseBody(r, request),
      e => this.processErrorBody(e)
    );
  }

  /**
   * No handling, simple pass through that returns the initial error.
   * @param {Error} error the error to pass through
   */
  // eslint-disable-next-line class-methods-use-this
  handleError(error: Error) {
    return Promise.reject(error);
  }

  /**
   * Create a thunk that handles the response given a request thunk
   * that fetches it.
   * @param {Function} requestThunk the thunk that fetches the
   *     response given a request
   */
  thunk(requestThunk: (r: RestRequest) => Promise<Response>) {
    return (request: RestRequest) => this.prepareRequest(request).then(
      preparedRequest => requestThunk(preparedRequest).then(
        (response: Response) => this.handleResponse(response, preparedRequest),
        (error: Error) => this.handleError(error)
      )
    );
  }
}
