import ApiError from './ApiError';
import RestRequest from './RestRequest';
import RestResponse from './RestResponse';
import ParsingError from './ParsingError';

/**
 * @module StatusError
 */

/**
 * A specialised error class for handling error or unexpected HTTP status
 * codes.
 * @alias module:StatusError
 * @class
 */
export default class StatusError extends ApiError {
  response: Response | RestResponse
  expected: Array<string>
  related: Array<Error>
  status: number
  type: 'server' | 'client' | 'unexpected'
  body?: any
  upstreamStatus?: number

  /**
   * Create a status error given a request, response, a list of expected
   * statuses, a list of related errors and a body.
   *
   * The body is typically not set in the initial error: for it to be set,
   * the withBody function should be called. Related errors store other
   * errors that occured when handling this particular error, such as parsing
   * errors when parsing the body.
   *
   * The constructor also sets a type attribute on the error depending on
   * the status code:
   * - server for values 500 and above
   * - client for values between 400 and 499
   * - unexpected for any value below 400, i.e. this is normally a success
   *   code but it was not defined for this operation and is therefore not
   *   expected to be handled correctly by the client
   * @param {String} message the error message
   * @param {RestRequest} request the initial request
   * @param {(Response|RestResponse)} response the HTTP response
   * @param {Array<string>} expected the list of expected statuses
   * @param {Array<Error>} related the list of related errors
   * @param {any} body the parsed body or its raw text value
   */
  constructor(
    message: string,
    request: RestRequest,
    response: Response | RestResponse,
    expected: Array<string> = [],
    related: Array<Error> = [],
    body?: any
  ) {
    super(message, request);
    this.name = 'StatusError';
    this.response = response;
    this.expected = expected;
    this.related = related;
    this.status = response.status;
    if (this.status >= 500) {
      // server error
      this.type = 'server';
    } else if (this.status >= 400) {
      // client error
      this.type = 'client';
    } else {
      // everything else is unexpected
      this.type = 'unexpected';
    }
    this.body = body;
    if (this.body && this.body.upstream_status) {
      this.upstreamStatus = this.body.upstream_status;
    }
  }

  /**
   * Whether the error is transient. A status error is considered transient
   * if the status code is a server error in the range 5XX.
   * @returns {boolean} whether the error is transient
   */
  isTransient(): boolean {
    return (this.type === 'server');
  }

  /**
   * Parses the resonse's body and creates an error with that parsed body.
   * If this error already has a value for the body, it resolves to itself.
   * If the body can be parsed, the resulting response held by the error
   * will be a RestResponse.
   * @returns {Promise} a promise that resolves to an updated error
   */
  withBody(): Promise<StatusError> {
    if (this.body == null) {
      let result = null;
      if (this.response instanceof RestResponse) {
        result = Promise.resolve(new StatusError(
          this.message,
          this.request,
          this.response,
          this.expected,
          this.related,
          this.response.body
        ));
      } else {
        result = RestResponse.fromRequestResponse(this.request, this.response).then(
          (r: Response | RestResponse) => new StatusError(
            this.message,
            this.request,
            r,
            this.expected,
            this.related,
            r.body
          ),
          (e: ParsingError) => new StatusError(
            this.message,
            this.request,
            this.response,
            this.expected,
            [...this.related, e],
            e.body
          )
        );
      }
      return Promise.resolve(result);
    }
    return Promise.resolve(this);
  }
}
