const default_world = 'world';

export function hello(world: string = default_world): string {
  return `Hello ${world}!`;
}
