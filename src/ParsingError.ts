import ApiError from './ApiError';
import RestRequest from './RestRequest';

/**
 * @module ParsingError
 */

/**
 * A specialised error class for parsing errors caught when handling a
 * response body.
 * @alias module:ParsingError
 * @class
 */
export default class ParsingError extends ApiError {
  response: Response
  contentType: string
  body: string

  /**
   * Create a parsing error given a message, the original request that
   * fetched the response, the response that triggered the error, the
   * underlying cause, content type and body of the response.
   * @param {String} message the error message
   * @param {RestRequest} request the request that was used to fetch the response
   * @param {Response} response the request that triggered the error
   * @param {Error} cause the underlying parsing error
   * @param {String} contentType the content type used to attempt to parse
   * @param {String} body the text body of the response
   */
  constructor(
    message: string,
    request: RestRequest,
    response: Response,
    cause: Error,
    contentType: string,
    body: string
  ) {
    super(message, request, cause);
    this.name = 'ParsingError';
    this.response = response;
    this.contentType = contentType;
    this.body = body;
  }

  /**
   * Whether the error is transient, defaults to true for parsing
   * errors. Note that this error is considered transient because
   * it results from an API response where the body is inconsistent
   * with the content type. Fixing this probably requires fixing the
   * API back-end, therefore the client can retry without change
   * but the error may appear again is the back-end keeps sending
   * an incorrect response.
   * @returns {boolean} whether the error is transient
   */
  isTransient(): boolean {
    return true;
  }
}
