import RestRequest from "./RestRequest"

/**
 * @module ApiError
 */

/**
 * A specialised error class for errors thrown by API clients.
 * @alias module:ApiError
 * @class
 */
export default class ApiError extends Error {
  _message: string
  request: RestRequest
  cause?: Error
  causeInfo?: object
  causeMessage?: string

  /**
   * Create an API error given a message and the original request that
   * triggered the error.
   * @param {string} message the error message
   * @param {RestRequest} request the request that triggered the error
   * @param {Error} cause the underlying error that is the cause of this error
   * @param {object} causeInfo the additional info related to the error
   */
  constructor(message: string, request: RestRequest, cause?: Error, causeInfo?: object) {
    super(message);
    this.name = 'ApiError';
    this._message = message;
    this.request = request;
    this.cause = cause;
    this.causeInfo = causeInfo;
    if (cause) {
      this.causeMessage = cause.message;
    }
  }

  /**
   * Whether this error is transient. If it is transient, the client should
   * be able to retry what it was doing without any changes; if it is not
   * transient, the client can't retry without modifications.
   * @returns {boolean} whether the error is transient
   */
  isTransient(): boolean {
    return false;
  }
}
