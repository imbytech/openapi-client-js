import ApiError from './ApiError';
import RestRequest from './RestRequest';

/**
 * @module ConnectionError
 */

/**
 * A specialised error class for connection errors occuring when an API
 * is called.
 * @alias module:ConnectionError
 * @class
 */
export default class ConnectionError extends ApiError {
  /**
   * Create a new connection error given a mesage, the original request
   * that triggered the error and the underlying error being handled.
   * @param {String} message the error message
   * @param {RestRequest} request the request that triggered the error
   * @param {Error} cause the underlying error being handled
   */
  constructor(message: string, request: RestRequest, cause: Error) {
    super(message, request, cause);
    this.name = 'ConnectionError';
  }

  /**
   * Whether the error is transient, defaults to true for connection
   * errors.
   * @returns {boolean} whether the error is transient
   */
  isTransient(): boolean {
    return true;
  }
}
