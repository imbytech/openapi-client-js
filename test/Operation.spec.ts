import { describe, it } from 'mocha';
import { expect } from 'chai';
import RestRequest from '../src/RestRequest';
import Operation from '../src/Operation';


// Mock blob for testing
// class Blob {
//   type: string

//   constructor(type: string) {
//     this.type = type;
//   }
// }

describe('Operation', () => {
  describe('create', () => {
    describe('no parameters', () => {
      it('GET', () => {
        const op = new Operation('http://example.com', 'hello', 'get', {
          operationId: 'getHello'
        });
        expect(op.request({}, {})).to.deep.equal(
          new RestRequest(
            'getHello',
            'http://example.com',
            'hello',
            'GET',
            {
              headers: {
                Accept: 'application/json'
              }
            }
          )
        );
      });
  
      it('POST', () => {
        const op = new Operation('http://example.com', 'hello', 'post', {
          operationId: 'postHello'
        });
        expect(op.request({}, {})).to.deep.equal(
          new RestRequest(
            'postHello',
            'http://example.com',
            'hello',
            'POST',
            {
              headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
              }
            }
          )
        );
      });
    });
  
    describe('with parameters', () => {
      it('query parameters', () => {
        const op = new Operation('http://example.com', 'hello', 'get', {
          operationId: 'getHello'
        });
        expect(op.request({
          name: 'World'
        }, {})).to.deep.equal(
          new RestRequest(
            'getHello',
            'http://example.com',
            'hello?name=World',
            'GET',
            {
              headers: {
                Accept: 'application/json'
              }
            }
          )
        );
      });
  
      it('path parameters', () => {
        const op = new Operation('http://example.com', 'person/{id}', 'get', {
          operationId: 'getPersonId',
          parameters: [{
            name: 'id',
            in: 'path'
          }]
        });
        expect(op.request({
          id: '1ec4'
        }, {})).to.deep.equal(
          new RestRequest(
            'getPersonId',
            'http://example.com',
            'person/1ec4',
            'GET',
            {
              headers: {
                Accept: 'application/json'
              }
            }
          )
        );
      });
  
      it('path and query parameters', () => {
        const op = new Operation('http://example.com', 'person/{id}', 'get', {
          operationId: 'getPersonId',
          parameters: [{
            name: 'id',
            in: 'path'
          }]
        });
        expect(op.request({
          id: '1ec4',
          name: 'World'
        }, {})).to.deep.equal(
          new RestRequest(
            'getPersonId',
            'http://example.com',
            'person/1ec4?name=World',
            'GET',
            {
              headers: {
                Accept: 'application/json'
              }
            }
          )
        );
      });
  
      it('body parameters', () => {
        const op = new Operation('http://example.com', 'hello', 'post', {
          operationId: 'postHello'
        });
        expect(op.request({
          name: 'World'
        }, {})).to.deep.equal(
          new RestRequest(
            'postHello',
            'http://example.com',
            'hello',
            'POST',
            {
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
              },
              body: '{"name":"World"}'
            }
          )
        );
      });
    });
  });
})

// describe('fetch response', () => {
//   it('resolves the response on 2XX with default spec', async () => {
//     // fetchMock.get('http://example.com/hello', {
//     //   status: 200,
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   },
//     //   body: 'Hello, world!'
//     // });
//     const op = new Operation('http://example.com', 'hello', 'get', {
//       operationId: 'getHello'
//     });
//     const request = op.request({}, {});
//     const thunk = op.thunk(RestRequest.thunk());
//     const result = await thunk(request);
//     expect(result.status).to.equal(200);
//     expect(await result.body).to.equal('Hello, world!');
//   });

//   it('rejects the response on 4XX with default spec', async () => {
//     // fetchMock.get('http://example.com/invalid', {
//     //   status: 404,
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   },
//     //   body: 'Not found'
//     // });
//     const op = new Operation('http://example.com', 'invalid', 'get', {
//       operationId: 'getHello'
//     });
//     const request = op.request({}, {});
//     const thunk = op.thunk(RestRequest.thunk());
//     const error = await thunk(request).catch(e => Promise.resolve(e));
//     expect(error.name).to.equal('StatusError');
//     expect(error.type).to.equal('client');
//     expect(error.message).to.equal('API Error: 404 - Not Found');
//     expect(error.status).to.equal(404);
//     const errorWithBody = await error.withBody();
//     expect(errorWithBody.body).to.equal('Not found');
//   });

//   it('rejects the response on 202 with explicit spec', async () => {
//     // fetchMock.get('http://example.com/accepted', {
//     //   status: 202,
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   },
//     //   body: 'Accepted'
//     // });
//     const op = new Operation('http://example.com', 'accepted', 'get', {
//       operationId: 'getHello',
//       responses: {
//         // eslint-disable-next-line quote-props
//         '200': {}
//       }
//     });
//     const request = op.request({}, {});
//     const thunk = op.thunk(RestRequest.thunk());
//     const error = await thunk(request).catch(e => Promise.resolve(e));
//     expect(error.name).to.equal('StatusError');
//     expect(error.type).to.equal('unexpected');
//     expect(error.message).to.equal('Expected one of [200], got: 202 - Accepted');
//     expect(error.status).to.equal(202);
//     const errorWithBody = await error.withBody();
//     expect(errorWithBody.body).to.equal('Accepted');
//   });

//   it('accepts the response on 200 with default spec', async () => {
//     // fetchMock.get('http://example.com/helloDefault', {
//     //   status: 200,
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   },
//     //   body: 'Hello, world!'
//     // });
//     const op = new Operation('http://example.com', 'helloDefault', 'get', {
//       operationId: 'getHello',
//       responses: {
//         default: {}
//       }
//     });
//     const request = op.request({}, {});
//     const thunk = op.thunk(RestRequest.thunk());
//     const result = await thunk(request);
//     expect(result.status).to.equal(200);
//     expect(result.body).to.equal('Hello, world!');
//   });

//   it('accepts the response on 200 with explicit spec', async () => {
//     // fetchMock.get('http://example.com/hello200', {
//     //   status: 200,
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   },
//     //   body: 'Hello, world!'
//     // });
//     const op = new Operation('http://example.com', 'hello200', 'get', {
//       operationId: 'getHello',
//       responses: {
//         // eslint-disable-next-line quote-props
//         '200': {}
//       }
//     });
//     const request = op.request({}, {});
//     const thunk = op.thunk(RestRequest.thunk());
//     const result = await thunk(request);
//     expect(result.status).to.equal(200);
//     expect(result.body).to.equal('Hello, world!');
//   });

//   it('accepts the response on 200 and parses JSON', async () => {
//     // fetchMock.get('http://example.com/helloJson', {
//     //   status: 200,
//     //   headers: {
//     //     'Content-Type': 'application/json'
//     //   },
//     //   body: '{"message":"Hello, world!"}'
//     // });
//     const op = new Operation('http://example.com', 'helloJson', 'get', {
//       operationId: 'getHello'
//     });
//     const request = op.request({}, {});
//     const thunk = op.thunk(RestRequest.thunk());
//     const result = await thunk(request);
//     expect(result.status).to.equal(200);
//     expect(result.body).to.equal({
//       message: 'Hello, world!'
//     });
//   });

//   it('returns a connection error when the request fails', async () => {
//     // fetchMock.get('http://example.com/helloError', {
//     //   throws: new Error('No network')
//     // });
//     const op = new Operation('http://example.com', 'helloError', 'get', {
//       operationId: 'getHello'
//     });
//     const request = op.request({}, {});
//     const thunk = op.thunk(RestRequest.thunk());
//     const result = await thunk(request).catch(e => Promise.resolve(e));
//     expect(result.name).to.equal('ConnectionError');
//     expect(result.message).to.equal('Connection error: No network');
//   });
// });

// describe('resolveContentType', () => {
//   it('no change when method is GET', () => {
//     const operation = new Operation('http://example.com', 'hello', 'get', {
//       operationId: 'getHello'
//     });
//     const options = {};
//     expect(operation.resolveContentType(options)).to.equal(options);
//   });

//   it('no change when content type specified', () => {
//     const operation = new Operation('http://example.com', 'hello', 'post', {
//       operationId: 'postHello'
//     });
//     const options = {
//       headers: {
//         'Content-Type': 'text/plain'
//       },
//       body: 'The quick brown fox'
//     };
//     expect(operation.resolveContentType(options)).to.equal(options);
//   });

//   it('defaults to application/json when nothing specified', () => {
//     const operation = new Operation('http://example.com', 'hello', 'post', {
//       operationId: 'postHello'
//     });
//     const options = {
//       body: {
//         foo: 'bar'
//       }
//     };
//     expect(operation.resolveContentType(options)).to.equal({
//       ...options,
//       headers: {
//         'Content-Type': 'application/json'
//       }
//     });
//   });

//   it('uses the type of the blob when present', () => {
//     const operation = new Operation('http://example.com', 'hello', 'post', {
//       operationId: 'postHello'
//     });
//     const options = {
//       body: new Blob('image/jpeg')
//     };
//     expect(operation.resolveContentType(options)).to.equal({
//       ...options,
//       headers: {
//         'Content-Type': 'image/jpeg'
//       }
//     });
//   });
// });

// describe('resolveAccept', () => {
//   it('defaults to application/json when no headers are provided', () => {
//     const operation = new Operation('http://example.com', 'hello', 'get', {
//       operationId: 'getHello'
//     });
//     const options = {};
//     expect(operation.resolveAccept(options)).to.equal({
//       headers: {
//         Accept: 'application/json'
//       }
//     });
//   });

//   it('defaults to application/json when no Accept header is provided', () => {
//     const operation = new Operation('http://example.com', 'hello', 'post', {
//       operationId: 'postHello'
//     });
//     const options = {
//       headers: {
//         'Content-Type': 'application/json'
//       }
//     };
//     expect(operation.resolveAccept(options)).to.equal({
//       headers: {
//         'Content-Type': 'application/json',
//         Accept: 'application/json'
//       }
//     });
//   });

//   it('leaves the header alone when provided', () => {
//     const operation = new Operation('http://example.com', 'hello', 'get', {
//       operationId: 'getHello'
//     });
//     const options = {
//       headers: {
//         Accept: 'image/jpeg'
//       }
//     };
//     expect(operation.resolveAccept(options)).to.equal(options);
//   });
// });
