import { describe, it } from 'mocha';
import { expect } from 'chai';
// import RestRequest from '../src/RestRequest';
// import RestResponse from '../src/RestResponse';

describe('RestResponse', () => {
  describe('Canary', () => {
    it('should run expect', () => {
      expect(true).to.equal(true)
    });
  });
})

// describe('create from request and response', () => {
//   it('returns a text body', async () => {
//     // fetchMock.get('http://example.com/helloText', 'The quick brown fox');
//     const request = new RestRequest('getHello', 'http://example.com/', 'helloText');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(
//       response => RestResponse.fromRequestResponse(request, response)
//     );
//     expect(result.request).to.equal(request);
//     expect(result.status).to.equal(200);
//     expect(result.body).to.equal('The quick brown fox');
//   });

//   it('returns a JSON body', async () => {
//     // fetchMock.get('http://example.com/helloJson', {
//     //   status: 200,
//     //   body: '{"foo":"bar"}',
//     //   headers: {
//     //     'Content-Type': 'application/json'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'helloJson');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(
//       response => RestResponse.fromRequestResponse(request, response)
//     );
//     expect(result.request).to.equal(request);
//     expect(result.status).to.equal(200);
//     expect(result.body).to.equal({
//       foo: 'bar'
//     });
//   });

//   it('returns a parsing error', async () => {
//     // fetchMock.get('http://example.com/helloConfused', {
//     //   status: 200,
//     //   body: 'The quick brown fox',
//     //   headers: {
//     //     'Content-Type': 'application/json'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'helloConfused');
//     const thunk = RestRequest.thunk();
//     const error = await thunk(request).then(
//       response => RestResponse.fromRequestResponse(request, response).catch(
//         e => Promise.resolve(e)
//       )
//     );
//     expect(error.name).to.equal('ParsingError');
//     expect(error.body).to.equal('The quick brown fox');
//     expect(error.contentType).to.equal('application/json');
//   });
// });
