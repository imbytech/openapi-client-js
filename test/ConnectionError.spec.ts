import { describe, it } from 'mocha';
import { expect } from 'chai';
// import RestRequest from '../src/RestRequest';

describe('ConnectionError', () => {
  describe('Canary', () => {
    it('should run expect', () => {
      expect(true).to.equal(true)
    });
  });
})

// describe('Connection error', () => {
//   it('fills in details properly', async () => {
//     // fetchMock.mock('http://example.com/helloError', {
//     //   throws: new Error('No network')
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'helloError');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).catch(
//       error => Promise.resolve(error)
//     );
//     expect(result.name).to.equal('ConnectionError');
//     expect(result.message).to.equal('Connection error: No network');
//     expect(result.request).to.equal(request);
//     expect(result.cause.message).to.equal('No network');
//   });
// });
