import { describe, it } from 'mocha';
import { expect } from 'chai';
import RestRequest from '../src/RestRequest';

// describe('fetch from RestRequest thunk', () => {
//   it('returns a JSON body', async () => {
//     // fetchMock.get('http://example.com/hello', { example: 'ok' });
//     const request = new RestRequest('getHello', 'http://example.com/', 'hello');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(resp => resp.json());
//     expect(result).to.deep.equal({ example: 'ok' });
//   });

//   it('returns an error', async () => {
//     // fetchMock.get('http://example.com/invalid', {
//     //   throws: new Error('No network')
//     // });
//     const request = new RestRequest('getInvalid', 'http://example.com/', 'invalid');
//     const thunk = RestRequest.thunk();
//     const error = await thunk(request).catch(e => Promise.resolve(e));
//     expect(error.name).to.equal('ConnectionError');
//     expect(error.message).to.equal('Connection error: No network');
//   });

//   it('base URL with partial path and no slash on sub-path', async () => {
//     // fetchMock.get('http://example.com/v1/hello1', { example: 'ok' });
//     const request = new RestRequest('getHello1', 'http://example.com/v1', 'hello1');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(resp => resp.json());
//     expect(result).to.deep.equal({ example: 'ok' });
//   });

//   it('base URL with partial path and initial slash on sub-path', async () => {
//     // fetchMock.get('http://example.com/v1/hello2', { example: 'ok' });
//     const request = new RestRequest('getHello2', 'http://example.com/v1', '/hello2');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(resp => resp.json());
//     expect(result).to.deep.equal({ example: 'ok' });
//   });

//   it('base URL with partial path and no sub-path', async () => {
//     // fetchMock.get('http://example.com/v2', { example: 'ok' });
//     const request = new RestRequest('getV2', 'http://example.com/v2');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(resp => resp.json());
//     expect(result).to.deep.equal({ example: 'ok' });
//   });
// });

describe('RestRequest', () => {
  describe('map', () => {
    it('maps the path', () => {
      const request = new RestRequest('getGreetingsId', 'http://example.com', 'greetings/{id}');
      expect(
        request.mapPath(path => path.replace('{id}', 'hi'))
      ).to.deep.equal(
        new RestRequest('getGreetingsId', 'http://example.com', 'greetings/hi', 'GET', {})
      );
    });
  
    it('maps the options', () => {
      const request = new RestRequest('getGreetingsId', 'http://example.com', 'greetings/{id}');
      expect(
        request.mapOptions((options: any) => ({
          ...options,
          headers: {
            ...options.headers,
            'Content-Type': 'application/json'
          }
        }))
      ).to.deep.equal(
        new RestRequest(
          'getGreetingsId',
          'http://example.com',
          'greetings/{id}',
          'GET',
          {
            headers: {
              'Content-Type': 'application/json'
            }
          }
        )
      );
    });
  });
})
