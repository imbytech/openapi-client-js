import { describe, it } from 'mocha';
import { expect } from 'chai';
import { hello } from '../src/hello';

describe('Hello, world!', () => {
  it('should return the default greeting', () => {
    const greeting = hello();
    expect(greeting).to.equal('Hello world!')
  });

  it('should return a custom greeting', () => {
    const greeting = hello('openapi');
    expect(greeting).to.equal('Hello openapi!')
  });
});
