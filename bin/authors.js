#!/usr/bin/env node

const { exec } = require('child_process');
const process = require('process');
const fs = require('fs');

function processOutput(error, stdout, stderr) {
  if(error) {
    console.error(`exec error: ${error}`);
    process.exit(1);
  }
  if(stderr) {
    console.error(stderr);
  }
  if(stdout) {
    // split into lines, keep the author ones and remove headers
    const lines = stdout
      .split('\n')
      .filter(line => line.startsWith('Author:'))
      .map(line => line.replace(/^Author:\s+/, ''));
    // remove duplicates
    const authors = Array.from(new Set(lines));
    // Add a carriage return at the end of each author and
    // join into a content string
    // Doing it this way ensures a final carriage return at
    // the end of the file
    const content = authors
      .map(author => `${author}\n`)
      .join('');
    fs.writeFileSync('AUTHORS', content);
  } else {
    console.error(`no output`);
    process.exit(2);
  }
}

// pipe grep in the exec command to get the OS to do some
// initial filtering and reduce the JS workload
exec("git log | grep 'Author:'", processOutput);
